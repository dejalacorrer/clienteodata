import { Component, ChangeDetectorRef } from '@angular/core';
import { CajasService } from './servicios/cajas.service';
import { Cajas } from './modelos';


@Component({
  selector: 'clio-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  valor: string;

  unacaja: Cajas;

  mensaje: string;

  hayentidad: boolean = false;

  esagregar: boolean = false;

  constructor( private cajasService: CajasService ) {

  }

  async BuscarEnElServidor() {
    
    try {
      this.hayentidad = false;
      this.mensaje = '';
      this.unacaja = await this.cajasService.BuscarCaja( this.valor );
      this.hayentidad = true;

    }
    catch( e ) {
      this.mensaje = 'No se encuentra / Hay un error';
    }
  }
  
  AgregarCaja() {

    this.unacaja = {
      id: 0,
      Nombre: '',
      Caracteristicas: ''
    }

    this.esagregar = true;
    this.hayentidad = true;

  }

  Cancelar() {
    this.hayentidad = false;
    this.esagregar = false;

    this.valor = '';

  }

  async GuardarCaja() {


    // Verifico al menos el nombre
    if ( ('' +this.unacaja.Nombre).length < 5 ) {
      this.mensaje = 'La caja debe tener un Nombre Valido';
      return;
    }

    try {

        if ( this.esagregar ) {

          await this.cajasService.CrearCaja( this.unacaja );
          this.mensaje = 'La caja ha sido agregada';
        } else {
          await this.cajasService.ActualizarCaja( this.unacaja );
          this.mensaje = 'La caja ha sido actualizada';
        }

        this.hayentidad = false;
        this.valor = '';
  
      }
      catch( e ) {
        if ( this.esagregar ) {
          this.mensaje = 'No se pudo agregar la caja en el servidor';      
        }
        else {
          this.mensaje = 'No se pudo actualizar la caja en el servidor';      
        }
       
      }

  }

  async EliminarCaja() {

    try {
      await this.cajasService.EliminarCaja( this.unacaja );
      this.hayentidad = false;
      this.valor = '';
      this.mensaje = 'La caja ha sido eliminada';
    }
    catch( e ) {
      this.mensaje = 'No se pudo eliminar la caja en el servidor';      
    }

  }

}
