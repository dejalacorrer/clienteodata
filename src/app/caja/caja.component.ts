import { Component, ChangeDetectorRef, Input } from '@angular/core';

import { Cajas } from '../modelos';


@Component({
  selector: 'clio-caja',
  templateUrl: './caja.component.html',
  styleUrls: ['./caja.component.scss']
})
export class CajaComponent {


	@Input()
	caja: Cajas;


  constructor( private cd: ChangeDetectorRef ) {

  }


}
