import { Injectable } from "@angular/core";
import { Cajas } from '../modelos';


declare var odatajs:any;

@Injectable({
	providedIn: 'root'
})
export class CajasService {


  puertoServidor = 8080;

	servicio = "DemostracionServicio.svc";
	
	servicioraiz = 'http://localhost:' + this.puertoServidor +  "/" + this.servicio + "/"

	oHeaders = {
		'Accept': 'application/json',
	 'Cache-Control': 'max-age=0',
		"Odata-Version": "4.0",
		"OData-MaxVersion": "4.0"
	};

	private hacerRequerimiento( subservicio: string, data: any, metodo: string ) {
		let requerimiento =   {
			headers: this.oHeaders,
		// requestUri: "http://services.odata.org/OData/OData.svc/Categories",
		// "http://odatasampleservices.azurewebsites.net/V4/OData/OData.svc/Categories"
			method: metodo,
		 requestUri:  this.servicioraiz + subservicio,
		 data,
	 };

	 return requerimiento;
	}

	private odataLeerColecciones( param: string ) {
		return this.odataRead( param, 'GET' );
	}

	private odataRead( param: string, metodo: string ) {

		let requerimiento = this.hacerRequerimiento( param, null, metodo );

		return new Promise<any>( (resuelve, rejecta  ) =>{

			odatajs.oData.read( requerimiento, ( data: any )=>{

			if ( data ) {
					resuelve( data );
				}
				else {
					rejecta( 'No se recibieron datos del servidor' );
				}
				return;
			},
				( errores: any )=>{
				// Fallo
				rejecta( 'No se encuentra / Hubo un error' );
			});

		});

	}

	private async odataLeerEntidad( param: string ) {
		return await this.odataRead( param, 'GET' );
	}

	private async odataCrearEntidad(param: string, data: any ) {

		return await this.odataRequest( param, data, 'POST');
	}

	private async odataActualizarEntidad( param: string, data: any ) {
		return await this.odataRequest( param, data, 'PATCH');
	}

	private async odataEliminarEntidad( param: string ) {
		return await this.odataRequest( param, null, 'DELETE');
	}

	private odataRequest( param: string, dataEnvio: any, metodo ) {

		let requerimiento = this.hacerRequerimiento( param, dataEnvio, metodo );

		return new Promise<any>( (resuelve, rejecta  ) =>{

			odatajs.oData.request( requerimiento, ( data: any )=>{
			// Correcto
			// document.getElementById("simpleRead").innerHTML = JSON.stringify(data, undefined, 2);
			if ( data ) {
					// this.valorDevuelto = data.Nombre + ' ( ' + data.Caracteristica + ' )';
					resuelve( data );
				}
				else {
					resuelve();
				}
			},
				( errores: any )=>{
				// Fallo
				rejecta( 'No se encuentra / Hubo un error' );
			});

		});

	}

	async BuscarCaja( id: string ) {

		let param = 'Cajas('+parseInt(id)+')';

		return await this.odataLeerEntidad( param );
	}
	
	async CrearCaja( caja: Cajas ) {

		let param = 'Cajas';

		return await this.odataCrearEntidad( param, caja );
	}

	async ActualizarCaja( caja: Cajas ) {
		let param = 'Cajas('+ caja.id +')';

		return await this.odataActualizarEntidad( param, caja );

	}

	async EliminarCaja( caja: Cajas ) {

		let param = 'Cajas(' + caja.id + ')';
		return await this.odataEliminarEntidad( param )

	}

}

