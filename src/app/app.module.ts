import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CajaComponent } from './caja/caja.component';


import { CajasService } from './servicios/cajas.service';


let COMPONENTES = [
  AppComponent,
  CajaComponent,
];

let SERVICIOS = [
  CajasService,
]

@NgModule({
  declarations: [
    ...COMPONENTES
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [
    ...SERVICIOS
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
